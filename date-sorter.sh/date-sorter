#!/usr/bin/env sh
# shellcheck shell=sh # Written to comply with IEEE Std 1003.1-2017 (POSIX)

#@ Released under the terms and conditions of the Expat License <https://mit-license.org>
#@ Copyright (C) Jacob Hrbek <kreyren@rixotstudio.cz>

### Organize all parsed pathnames by date to the OUTPUT directory
###
### Expected file hierarchy in the output directory:
###     [[ output directory ]]
###       ├── YYYY
###       │   └── YYYY-MM-DD
###       │       └── files
###
###
### Expected command usecase:
###     $ path/to/this/script file-B.txt file-B.txt -o path/to/output/directory

# NOT DESIGNED FOR PRODUCTION AND/OR MISSION CRITICAL ENVIROMNET!

set -e # Exit on false

# Helpers
## Common Assertation with custom output message
${COMMAND:-command} -v ${DIE:-die} 1>/dev/null || die() {
	${PRINTF:-printf} 'FATAL: %s\n' "${2:-"Message for 'die' not specified"}"
	exit 1
}

## Fixme helper
${COMMAND:-command} -v ${FIXME:-fixme} 1>/dev/null || fixme() {
	${PRINTF:-printf} 'FIXME: %s\n' "$1"
}

## Helper for debug messages
${COMMAND:-command} -v ${DEBUG:-debug} 1>/dev/null || debug() {
	[ "$DEBUG" != 1 ] || ${PRINTF:-printf} 'DEBUG: %s\n' "$2"
}

# Check for dependencies
## STAT - For checking the file date metadata
${COMMAND:-command} -v ${STAT:-stat} 1>/dev/null || die 3 "Command 'stat' is not available in this environment which is required to get usage metadata from files for sorting"

## SED - For Regular Expressions
${COMMAND:-command} -v ${SED:-sed} 1>/dev/null || die 3 "Command 'sed' is not available in this environment which is required for regular expressions"

## MKDIR - For making directories
${COMMAND:-command} -v ${MKDIR:-mkdir} 1>/dev/null || die 3 "Command 'mkdir' is not available in this environment which is required for making directories" 

# Process command line arguments
while [ $# -gt 0 ]; do case "$1" in
	/*|^[^./][^/]+(/[^/]*)?$) # Checking for files with full and relative path
		files="$files $1"
		;;
	"-o"|"--output") # Declare the output directory
		# Check if the parsed $2 is a valid directory
		[ -d "$2" ] || {
			case "$LANG" in
				en-*|*) ${DIE:-die} 1 "Output directory '$2' is not valid directory"
			esac
		}
		outputDir="$2"
		shift 1
		;;
	"-h")
		${FIXME:-fixme} "Short help message"
		exit 0
		;;
	"--help")
		${FIXME:-fixme} "Long help message"
		exit 0
		;;
	*)
		case "$LANG" in
			en-*|*) ${DIE:-die} 255 "Unexpected argument '$1' is not recognized, exitting for safety"
		esac
esac; shift 1; done

[ -n "$outputDir" ] || die 3 "The output directory has not been set, use the mandatory -o|--output option followed by path to a valid directory"

regex="^([0-9]+)-([0-9]+)-([0-9]+).*" # YEAR MONTH DAY

# NOTE(Krey): Using the '${file//*\/}' to strip the absolute path

# Process the files
for file in $files; do
	# Declare YEAR MONTH and DAY variable and check that the commands didn't return empty string
	## YEAR
	year="$(${STAT:-stat} --format="%w" "$file" | ${SED:-sed} -E "s/$regex/\1/")"
	[ -n "$year" ] || {
		case "$LANG" in
			en-*|*) ${DIE:-die} 1 "The combination of 'stat' and 'sed' failed to get a sane 'year' variable: $year"
		esac
	}

	## MONTH
	month="$(${STAT:-stat} --format="%w" "$file" | ${SED:-sed} -E "s/$regex/\2/")"
	[ -n "$month" ] || {
		case "$LANG" in
			en-*|*) ${DIE:-die} 1 "The combination of 'stat' and 'sed' failed to get a sane 'month' variable: $month"
		esac
	}

	## DAY
	day="$(${STAT:-stat} --format="%w" "$file" | ${SED:-sed} -E "s/$regex/\3/")"
	[ -n "$day" ] || {
		case "$LANG" in
			en-*|*) ${DIE:-die} 1 "The combination of 'stat' and 'sed' failed to get a sane 'day' variable: $day"
		esac
	}

	# Make sure the YEAR directory is present in the output dir
	[ -d "$outputDir/$year" ] || ${MKDIR:-mkdir} --parents "$outputDir/$year"

	# Make sure the sub-directory of YEAR-MONTH-DAY is presend in the YEAR directory
	[ -d "$outputDir/$year/$year-$month-$day" ] || ${MKDIR:-mkdir} --parents "$outputDir/$year/$year-$month-$day"

	# Copy the relevant file to the relevant directory
	[ -f "$outputDir/$year/$year-$month-$day/${file//*\/}" ] || ${CP:-cp} "$file" "$outputDir/$year/$year-$month-$day/${file//*\/}"

	# Output status
	${DEBUG:-debug} 1 "file '$file' created '$year-$month-$day' has been copied in '$outputDir/$year/$year-$month-$day/${file//*\/}'"
done 
